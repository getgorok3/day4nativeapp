import { createStore, combineReducers} from 'redux'
import TodosReducer from './TodosReducer'
import CompleteReducer from './CompletesReducer'

const reducers = combineReducers({                  //รับ ตัวเปรเป็น obj ใช้ในกรณี มี reducers หลายอัน
    todos: TodosReducer,                            //
    CompleteReducer: CompleteReducer
});

const store = createStore(reducers);
// const cur = store.getState();
// console.log(cur)
export default store;