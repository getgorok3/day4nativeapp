import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'

class Page1 extends Component {
   
    render() {
        console.log(this.props.todos)
        const { todos, completes,addTodO } = this.props //ดึงมาจากprops เรียก Deconstructing
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => {
                    addTodO('Todo: ' + Math.random());
                 }}>
                    <Text>Add Number to Todo</Text>
                </TouchableOpacity>
               
                <Text>
                    Latest todo is: {todos[todos.length - 1]}
                </Text>

            </View>
        )
    }
}
const mapStateToProps = (state) => {          // อ่านของในstate in store เพื่อเอามาให้ props ของ component
    return {
        todos: state.todos,                  // props: state from store === subscribe
        completes: state.completes
    }
}
const mapDispatchToProps = (dispatch) => {                     // เรียกใช้ action เพื่อ เปลี่ยนแปลง state 
    return{
        addTodO: (topic)=>{                       // fn In this class : dispatch that match to reducer
            dispatch({
                type: 'ADD_TODO',                   //ทุก action ต้องตัวพิมพ์ใหญ่ cap
                topic: topic
            })
        }                       
    }
}
export default connect(mapStateToProps, mapDispatchToProps
)(Page1)        //connect call fn ดูว่าอันไหนเชื่อมอันไหน == สามารถใช้ stateได้ และ อีกอัน ดูว่า cm นี้ ใช้ action อะไรบ้าง